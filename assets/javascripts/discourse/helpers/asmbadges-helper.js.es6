import { createWidget } from 'discourse/widgets/widget';

createWidget('asm-badges', {
  tagName: 'div.badges',

  html(attrs) {
  	console.log(attrs);
  	$.ajax({
        url: "https://admin.amazing.com/api/member_badges_display/" + attrs + "/10/",
        crossDomain: true,
        success: function (response) {
        	$('.amazing-badge-section').html(response);
        },
        error: function (ErrorResponse) {
        	console.log('Error:'+ErrorResponse);
        	return;
        }
      });
  }
});


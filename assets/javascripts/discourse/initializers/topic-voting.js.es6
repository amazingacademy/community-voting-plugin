import TopicListItem from 'discourse/components/topic-list-item';

export default {
  name: "topic-voting",

  initialize: function (container) {
    var topic_ids = [];
    var topic_ids_length = 20;
    var getVotesTimeout;

    function vote(params) {
      var url = '/amazing/voting/' + params.topic.id + '/' + params.option;
      
      jQuery.ajax(url, {type: 'POST'})
        .then(function(data){
          var el = $("table").find("[data-topic-id='" + params.topic.id + "']");

          el.find('.score').html(nFormatter(data.score));

          var votingWidth = el.find('.voting').outerWidth();
          el.find('.voting-spacer').css({'padding-left': votingWidth + 8});

          const btns = $("table").find("[data-topic-id='" + params.topic.id + "']").find(".arrow");
          btns.find('svg').attr('class', '');

          const btn = $("table").find("[data-topic-id='" + params.topic.id + "']").find(".arrow." + params.option);
          btn.find('svg').attr('class', 'active');
        });
    }

    function nFormatter(num) {
         if (num >= 1000000000 || num <= -1000000000) {
            return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
         }
         if (num >= 1000000 || num <= -1000000) {
            return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
         }
         if (num >= 1000 || num <= -1000) {
            return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
         }
         return num;
    }

    function getVotes() {
      if (topic_ids.length === 0) {
        if (getVotesTimeout) { clearTimeout(getVotesTimeout);}
        return;
      }

      var ids = topic_ids;
          topic_ids = [];

          var url = '/amazing/voting/' + ids.join(',') + '/scores'

          jQuery.ajax(url, {type: 'GET'})
            .then(function(data) {
              for (var topic_id in data) {
                var el = $("table").find("[data-topic-id='" + topic_id + "']").not('.has-excerpt');

                if (el.closest('.user-right').length === 0) { //if not PM
                  el.find('.score').html(nFormatter(data[topic_id].score));

                  var votingWidth = el.find('.voting').outerWidth();

                  el.find('.voting-spacer').css({'padding-left': votingWidth + 8});

                  if (data[topic_id].voted) {
                    const btn = $("table").find("[data-topic-id='" + topic_id + "']").find(".arrow." + data[topic_id].vote);
                    btn.find('svg').attr('class', 'active');
                  }
                }
              }

              return true;
            });
    }


    TopicListItem.reopen({
      click: function(e) {

        let target = $(e.target);

        let option = target.attr('am-vote');

        const topic = this.get('topic');

        if (option === 'up') {
          vote({
            option: 'up',
            topic: topic
          });
        }

        if (option === 'down') {
          vote({
            option: 'down',
            topic: topic
          });
        }

        this._super(e);
      },
      scores: function() {
        var topic = this.get('topic');

        if (topic_ids.length < topic_ids_length) {
          if (getVotesTimeout) { clearTimeout(getVotesTimeout);}

          topic_ids.push(topic.id);

          getVotesTimeout = setTimeout(function() { 
            getVotes();
          }, 3000);
        }

        if (topic_ids.length === topic_ids_length) {
          //If we've reached our topic_ids_length, getVotes immediately.
          getVotes();
        }
      }
    });

  }
};
Discourse::Application.routes.append do
	post "amazing/voting/:topic_id/:option" => "voting#vote"
	get "amazing/voting/:topic_ids/scores" => "voting#scores"
end

require 'net/http'

UserSerializer.class_eval do

  attributes :custom_badges

  # Get custom badges from Campus
  def custom_badges
    custom_name = username.gsub('_', '%20')
    url = URI.parse('http://campus.amazing.com/home/member_badges_display/'+custom_name)
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    res.body
  end

end

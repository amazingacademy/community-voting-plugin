TopicListItemSerializer.class_eval do

  attributes :custom_image, :custom_content

  def custom_content
    test_excerpt =  Nokogiri::HTML(object.first_post.cooked)
    img_src = test_excerpt.search('//text()').map(&:text).delete_if{|x| x !~ /\w/}
    img_src = ActionView::Base.full_sanitizer.sanitize(img_src.join(' ').gsub(/\s+/, ' '))
    img_src
  end

  def custom_image
    test_excerpt =  Nokogiri::HTML(object.first_post.cooked)
    img_src = test_excerpt.css('img').map{ |i| i['src'] }
    if img_src[0].is_a? String
      if !img_src[0].include? '/images'
        return img_src[0]
      end
    end
    return nil
  end

end

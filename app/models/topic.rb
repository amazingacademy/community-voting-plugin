class ::Topic
	has_reputation :votes, :source => :user  


	def votes
		reputation_for(:votes)
	end
end

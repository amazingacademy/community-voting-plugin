TopTopic.class_eval do

  def self.refresh_daily!
    transaction do
      remove_invisible_topics
      add_new_visible_topics

      update_counts_and_compute_scores_for(:daily)
    end

    #Repeating all code from the original method and adding the line below
    compute_final_numbers
  end

  def self.refresh_older!
    older_periods = periods - [:daily,:all]

    transaction do
      older_periods.each do |period|
        update_counts_and_compute_scores_for(period)
      end
    end

    compute_top_score_for(:all)

    #Repeating all code from the original method and adding the line below
		compute_final_numbers
  end


  #Here's where we store the voting numbers for each period
  #and also add the votes to the period's score.

  #We're using Discourse's original algorithm to define what is Top and what is not,
  #but ordering by vote. 

  #A topic with lower voting can be displayed above a topic with a higher voting if it
  #has "much more" views, likes and replies.
  #Also, for instance: a topic with 999999999 votes and 0 views will never be added to the Top List.
  def self.compute_final_numbers
		limit = SiteSetting.topics_per_period_in_top_page

		periods.each do |period|
			#Getting the first X (limit) Top Topics of this period
			@tts = TopTopic.order("#{period}_score" => :desc).first(limit)

			@tts.each do |tt|
				t = tt.topic

				#Getting the votes for this period and updating the period_votes_count
				if period === :all
					votes = t.evaluations(:votes).sum(:value)
				else
					votes = t.evaluations(:votes).where("updated_at > ?", start_of(period)).sum(:value)
					tt["#{period}_votes_count"] = votes
				end

				#Removing topics with negative reputation/voting
				if votes < 0
					tt.destroy
				else
					#Now we add the votes to the period_score that Discourse gave it
					tt["#{period}_score"] += votes
					tt.save!
				end
			end
    end
  end
end

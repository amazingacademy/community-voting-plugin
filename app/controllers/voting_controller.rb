class VotingController < ApplicationController
	skip_before_action :check_xhr
	
	def vote
		@topic = Topic.find(params[:topic_id])

		if (params[:option] === "up")
			vote = 1
		else
			vote = -1
		end

		@topic.add_or_update_evaluation(:votes, vote, current_user)

		render json: {status: "success", score: @topic.votes}
	end

	def scores
		@topics = Topic.where(id: params[:topic_ids].split(","))

		scores = {}

		@topics.each do |topic|
			vote = topic.evaluations(:votes).find_by_source_id(current_user.id)

			if vote
				voted = true
				if vote.value > 0
					vote = "up"
				else
					vote = "down"
				end
			else
				voted = false
			end

			scores[topic.id] = {score: topic.votes, voted: voted, vote: vote}
		end

		render json: scores
	end

end
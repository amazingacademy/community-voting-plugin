# name: voting
# about: Adds up/down voting to Discourse
# version: 1.0.7
# authors: Gustavo Scanferla, Carlos Urango
# url: https://bitbucket.org/amazingacademy/community-voting-plugin

gem 'activerecord-reputation-system', '3.0.1'


register_asset "javascripts/main.js"
register_asset "stylesheets/voting.scss"
register_asset "javascripts/discourse/templates/list/topic-list-item.raw.hbs"
register_asset "javascripts/discourse/templates/user/user.hbs"

Rails.configuration.assets.precompile += ['voting.css']

after_initialize do

	load File.expand_path("../app/models/topic.rb", __FILE__)
	load File.expand_path("../app/models/post.rb", __FILE__)
	load File.expand_path("../app/models/top_topic.rb", __FILE__)
	load File.expand_path("../app/serializers/topic_list_item_serializer.rb", __FILE__)
	load File.expand_path("../app/serializers/user_serializer.rb", __FILE__)
	load File.expand_path("../app/controllers/voting_controller.rb", __FILE__)
	load File.expand_path("../config/routes.rb", __FILE__)

end

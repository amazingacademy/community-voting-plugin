class VotingTopTopics < ActiveRecord::Migration
  PERIODS = [:yearly, :monthly, :weekly, :daily]

  def change

    PERIODS.each do |period|
    	add_column :top_topics, "#{period}_votes_count".to_sym, :integer, null: false, default: 0
    	add_index :top_topics, "#{period}_votes_count".to_sym, order: 'desc'
    end

  end
end

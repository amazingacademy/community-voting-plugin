# Voting plugin

Discourse plugin that adds **up/down voting** to topics. It also sorts `Top` topics based on the number of `votes`.

# How to install

Add `git clone https://scanferla@bitbucket.org/amazingacademy/community-voting-plugin.git` to your container's `app.yml` file:

```yml
hooks:
  after_code:
    - exec:
        cd: $home/plugins
        cmd:
          - mkdir -p plugins
          - git clone https://github.com/discourse/docker_manager.git
          - git clone https://scanferla@bitbucket.org/amazingacademy/community-voting-plugin.git
```

Rebuild the container: 

`cd /var/discourse`

`./launcher rebuild app`


Now we need to generate some migration files and then run the migration.

First, enter your Discourse container:

`./launcher enter app` (where `app` is the name of your container)

Then run:

`rails generate reputation_system`

`rake db:migrate`

# Top Topics
We're *sorting* topics based on a formula that takes into account the number `votes`, `replies`, `views` and `likes`. This sorted list is displayed on the `Top` page. We're using the *same* formula `Discourse` uses out of the box, but adding the `votes` number into the mix.

The `Top` section is `refreshed` *hourly* and *daily* (depending on the sub-section: `This month`, `Today`, etc...). You can *force* it to refresh immediately if you run `TopTopic.refresh!` from the `rails console`.
